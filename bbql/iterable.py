import operator

from builtins import filter as ifilter
from functools import total_ordering
from inspect import getmro

from past.builtins import basestring
from plyplus import PlyplusException

from bbql.exceptions import (UnqueryableFieldException,
                             InvalidQueryStringException,
                             InvalidSortStringException, InvalidPathException)
from bbql.parser import parser, BaseTransformer
from bbql.resolver import get_tokenized_field_value, get_field_value

# A dict mapping object types to a list of tuples, each mapping a dotted field
# path to the name of an attribute on the object.
#
# E.g.
#
# { <class 'blogs.models.Bytes'>: [
#     (('in',), 'bytes_in'),
#     (('out',), 'bytes_out')],
# }
klasses = {}


@total_ordering
class MinType(object):
    """Stub type that is always less than anything besides itself. Useful as
    a singleton to sort unsortable types together eg. None and int"""
    def __le__(self, other):
        return self is not other

    def __eq__(self, other):
        return self is other

Min = MinType()


def get_iterable_field_value(item, field_toks):
    klass = next((t for t in getmro(type(item)) if t in klasses), None)
    if klass:
        for k, col in klasses[klass]:
            if field_toks[:len(k)] == k:
                if callable(col):
                    item = col(item)
                else:
                    item = getattr(item, col)
                while callable(item):
                    item = item()
                tail = tuple(field_toks[len(k):])
                return (get_tokenized_field_value(item, tail)
                        if tail and item is not None
                        else item)
    raise InvalidPathException()


class Condition(object):
    def __init__(self, field, op, value):
        self.field = field
        self.op = op
        self.expected = value

    def __call__(self, item):
        """Takes an item from the iterable that is being filtered and tests
        its condition and returns `True` if the item matches the condition, or
        `False` otherwise.
        """
        try:
            val = get_field_value(item, self.field)
            return any(self.op(v, self.expected) for v in
                       (val if isinstance(val, (list, tuple, set)) else [val]))
        except InvalidPathException:
            raise UnqueryableFieldException(self.field)


class IterableTransformer(BaseTransformer):

    def expr(self, exp):
        icontains = lambda a, b: (isinstance(a, basestring) and
                                  isinstance(b, basestring) and
                                  b.lower() in a.lower())
        field, op, value = exp.tail
        return Condition(field, {
            '=': operator.eq,
            '!=': operator.ne,
            '~': icontains,
            '!~': lambda a, b: not icontains(a, b),
            '>': operator.gt,
            '>=': operator.ge,
            '<=': operator.le,
            '<': operator.lt}[op], value)

    def _and(self, exp):
        return lambda item: exp.tail[0](item) and exp.tail[1](item)

    def _or(self, exp):
        return lambda item: exp.tail[0](item) or exp.tail[1](item)

    def filter(self, iterable, query):
        try:
            tree = parser.parse(query)
        except PlyplusException as e:
            raise InvalidQueryStringException(e)
        else:
            return ifilter(self.transform(tree), iterable)

    def order_by(self, iterable, exp):
        m = self.sort_re.match(exp)

        def _field_value_sorter(i):
            # If the field value happens to be None a simple sort would throw
            # an exception in py3. This one-liner will replace None's with
            # a sentinel that is basically a sortable null
            field_value = get_field_value(i, field)
            return Min if field_value is None else field_value

        if m:
            direction, field = m.groups()
            return sorted(
                iterable, key=_field_value_sorter,
                reverse=('-' == direction))
        raise InvalidSortStringException(exp)


def filters(klass, fields):
    """Registers a query filter mapping for a queryable type.

    This function can be used as a transparent decorator.

    :param klass:   a filterable type.
    :param fields:  a list of query string field name to type attribute name
                    tuples. Every queryable field must be explicitly included
    """
    klasses[klass] = [(tuple(k.split('.')), v) for k, v in fields]
    return lambda func: func
