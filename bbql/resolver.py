from inspect import getmro

from bbql.exceptions import InvalidPathException


def get_field_value(item, path):
    """Takes a field strings from the query and resolves it to a property on
    the items in the iterable being filtered and returns its value so that the
    caller can test it against the query's conditions.

    The field must have been split into its constituents by the caller.

    :param path:        the public field (e.g. `'blog.user.id'`)
    :param item:        an item from the iterable
    :except InvalidPathException:   when the given path does not exist on the
                                    provided object
    :return:            the value of the item's attribute
    """
    try:
        return (get_tokenized_field_value(item, tuple(path.split('.')))
                if path else item)
    except InvalidPathException:
        # Rethrow so we can add the original path as the message:
        raise InvalidPathException(path)


def get_tokenized_field_value(item, field_toks):
    from bbql.iterable import klasses, get_iterable_field_value
    from bbql.orm import QBuilder, get_orm_field_value

    for t in getmro(type(item)):
        if t in QBuilder.registry:
            return get_orm_field_value(item, field_toks)
        elif t in klasses:
            return get_iterable_field_value(item, field_toks)
    raise InvalidPathException()
