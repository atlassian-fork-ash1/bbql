import six

from unittest import TestCase

from bbql import FilterException
from bbql import execute as runbbql
from bbql.exceptions import UnqueryableFieldException, InvalidPathException
from bbql.iterable import get_iterable_field_value

from blogs.models import Bytes, AccessLogEntry
from blogs.tests import take


class FieldResolutionTest(TestCase):

    def test_flat_field(self):
        item = Bytes(1, 2)
        self.assertEquals(1, get_iterable_field_value(item, ('in',)))
        self.assertEquals(2, get_iterable_field_value(item, ('out',)))
        with self.assertRaises(InvalidPathException):
            get_iterable_field_value(item, ('foo',))

    def test_unresolvable_nested(self):
        item = Bytes(1, 2)
        with self.assertRaises(InvalidPathException):
            get_iterable_field_value(item, tuple('foo.bar'.split('.')))
        with self.assertRaises(InvalidPathException):
            get_iterable_field_value(item, tuple('bytes_in.fubar'.split('.')))

    def test_nesting(self):
        item = AccessLogEntry('/path', 200, Bytes(100, 1024))
        self.assertEquals('/path', get_iterable_field_value(item, ('url',)))
        self.assertEquals(200, get_iterable_field_value(item, ('status',)))
        self.assertEquals(
            100, get_iterable_field_value(item, tuple('bytes.in'.split('.'))))
        self.assertEquals(
            1024, get_iterable_field_value(item, tuple('bytes.out'.split('.'))))

    def test_subclass(self):
        class MyBytes(Bytes):
            pass

        class MyAccessLogEntry(AccessLogEntry):
            pass

        item = MyAccessLogEntry('/path', 200, MyBytes(100, 1024))
        self.assertEquals('/path', get_iterable_field_value(item, ('url',)))
        self.assertEquals(200, get_iterable_field_value(item, ('status',)))
        self.assertEquals(
            100, get_iterable_field_value(item, tuple('bytes.in'.split('.'))))
        self.assertEquals(
            1024, get_iterable_field_value(item, tuple('bytes.out'.split('.'))))

    def test_callable(self):
        item = AccessLogEntry('/path', None,
                              lambda: Bytes(lambda: lambda: 100, 1024))
        self.assertEquals(None, get_iterable_field_value(item, ('status',)))
        self.assertEquals(
            100, get_iterable_field_value(item, tuple('bytes.in'.split('.'))))
        self.assertEquals(
            1024, get_iterable_field_value(item, tuple('bytes.out'.split('.'))))

    def test_custom_resolution(self):
        item = AccessLogEntry('/path', None, Bytes(1024, 2*1024**2))
        io = get_iterable_field_value(item, ('kb',))
        self.assertEquals(1, io.bytes_in)
        self.assertEquals(2048, io.bytes_out)


class FilteringTest(TestCase):
    def setUp(self):
        self.logs = [
            AccessLogEntry('/pa/th', 200, lambda: Bytes(100, 1024), []),
            AccessLogEntry('/path', 200, None, []),
            AccessLogEntry('/path', 404, Bytes(0, 1024), ['binary', 'exec']),
            AccessLogEntry(None, 102, Bytes(None, 1), ['binary']),
        ]

    def test_nesting(self):
        result = runbbql(self.logs, 'bytes.out = 1024')
        six.assertCountEqual(self, [self.logs[0], self.logs[2]],
                              list(result))

    def test_and(self):
        result = runbbql(self.logs, 'status = 404 and url = "/path"')
        six.assertCountEqual(self, self.logs[2:3], list(result))

    def test_or(self):
        result = runbbql(self.logs, 'status = 404 or bytes = null')
        six.assertCountEqual(self, self.logs[1:3], list(result))

    def test_and_or(self):
        result = runbbql(self.logs, '(status != null and status = 404 '
                                    'or bytes = null) and url ~ "/path"')
        six.assertCountEqual(self, self.logs[1:3], list(result))

    def test_contains(self):
        six.assertCountEqual(
            self,
            self.logs[1:3],
            list(runbbql(self.logs, 'url ~ "pat"')))
        six.assertCountEqual(
            self,
            self.logs[1:3],
            list(runbbql(self.logs, 'url ~ "pAt"')))
        six.assertCountEqual(
            self,
            self.logs[1:3],
            list(runbbql(self.logs, 'url ~ "pAt"')))
        six.assertCountEqual(
            self,
            [],
            list(runbbql(self.logs, 'url ~ null')))

    def test_not_contains(self):
        six.assertCountEqual(
            self, [self.logs[0], self.logs[3]],
            list(runbbql(self.logs, 'url !~ "pat"')))
        six.assertCountEqual(
            self, [self.logs[0], self.logs[3]],
            list(runbbql(self.logs, 'url !~ "pAt"')))

    def test_unfilterable_type(self):
        with self.assertRaises(UnqueryableFieldException):
            list(runbbql([object()], 'foo = "bar"'))

    def test_invalid_syntax(self):
        with self.assertRaises(FilterException):
            list(runbbql([object()], 'foo = """'))

    def test_lists(self):
        result = runbbql(self.logs, 'labels = "binary"')
        six.assertCountEqual(self, self.logs[2:4], list(result))

        result = runbbql(self.logs, 'labels ~ "bin"')
        six.assertCountEqual(self, self.logs[2:4], list(result))

        result = runbbql(self.logs, 'labels = "binary" and labels = "exec"')
        six.assertCountEqual(self, take([2], self.logs), list(result))

        result = runbbql(self.logs, 'labels ~ "foobar"')
        six.assertCountEqual(self, [], list(result))
