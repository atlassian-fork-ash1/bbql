# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=1024)),
                ('content', models.TextField()),
                ('status', models.CharField(default=b'2', max_length=128, choices=[(b'1', b'pending'), (b'2', b'published'), (b'3', b'redacted')])),
                ('commenting_closed', models.BooleanField(default=False)),
                ('author', models.ForeignKey(related_name=b'blogs', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
                ('author', models.ForeignKey(related_name=b'comments', to=settings.AUTH_USER_MODEL, null=True)),
                ('blog', models.ForeignKey(related_name=b'comments', to='blogs.Blog')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
