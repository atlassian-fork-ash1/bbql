BBQL: Bitbucket Query Language
==============================

This project contains the grammar and parser for the Bitbucket query language
used to provide filtering on API endpoints.


Testing
-------

BBQL is a Django app that comes with a test project fixture in which the tests
are run:

    $ cd tests/test_project
    $ ./manage.py test blogs.tests

The above commands run BBQL against Sqlite. When run in Pipelines, we also
test against Postgres. For this we maintain a custom Pipelines Docker
images that contains Postgres 9.3. The Dockerfile and miscellaneous files for
this live in the bitbucket-pipelines directory.

To rebuild this image:

    $ cd bitbucket-pipelines
    $ docker build -t bitbucket-pipelines .
    $ docker push bitbucket-pipelines
